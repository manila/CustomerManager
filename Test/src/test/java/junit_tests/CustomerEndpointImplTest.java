package junit_tests;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import resources.CustomerResource;

public class CustomerEndpointImplTest {
	private WebTarget webTarget;

	@Before
	public void setUp() {
		Client client = ClientBuilder.newClient();
		webTarget = client.target("http://02267-manila.compute.dtu.dk:8002");
	}	

	@Test
	public void givenCustomerPostWithCprNumber_returnPostWasSuccessful() {
		CustomerResource customerResource = new CustomerResource();
		customerResource.cprNumber = "1212822266";

		webTarget = webTarget.path("customers");

		Response response = webTarget.request().post(Entity.entity(customerResource, MediaType.APPLICATION_JSON));

		assertEquals(200, response.getStatus());
		response.close();
	}

}
