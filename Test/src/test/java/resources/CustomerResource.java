package resources;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class CustomerResource {
	@XmlElement 
	public String cprNumber;
}
