package dk.manila.customermanager.tools;


import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 */
@XmlRootElement(name = "request")
public class Request {
	private String cpr;
	private String fname;
	private String lname;
	public void setCpr(String cpr) {
		this.cpr = cpr;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getCpr() {
		return cpr;
	}
	public String getFname() {
		return fname;
	}
	public String getLname() {
		return lname;
	}

}