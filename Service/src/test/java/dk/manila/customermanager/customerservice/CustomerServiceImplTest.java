package dk.manila.customermanager.customerservice;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import dk.manila.customermanager.customerservice.CustomerServiceImpl;
import dk.manila.customermanager.queue.RabbitMqSender;
import dk.manila.customermanager.repository.CustomerRepository;
import dk.manila.customermanager.tools.Customer;

/**
 * 
 * @author Alexandre, Altug
 *
 */
public class CustomerServiceImplTest {
	private CustomerServiceImpl service;
	private String cpr;

	@Test 
	public void givenCprOfRegisteredCustomer_returnTrue() {
		service = new CustomerServiceImpl(new RabbitMqSender("dtu_pay_exchange", "rabbitmq", "fanout"),
				new CustomerRepository() {

					@Override
					public boolean contains(Customer customer) {
						return true; 
					}

					@Override
					public void addCustomer(Customer customer) {
					}
				});

		assertTrue(service.contains(cpr));
	}

	@Test
	public void givenCprOfNonRegisteredCustomer_returnFalse() {
		service = new CustomerServiceImpl(new RabbitMqSender("dtu_pay_exchange", "rabbitmq", "fanout"),
				new CustomerRepository() {

					@Override
					public boolean contains(Customer customer) {
						return false;
					}

					@Override
					public void addCustomer(Customer customer) {
					}
				});
		assertFalse(service.contains(cpr));
	}

}
