package dk.manila.customermanager.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import dk.manila.customermanager.tools.CprNumber;
import dk.manila.customermanager.tools.Customer;

/**
 * 
 * @author Alexandre, Altug
 *
 */
public class CustomerRepositoryTest {
	
	private CustomerRepositoryImpl repository;
	private Customer customer;
	@Before
	public void initialization() {
		repository = new CustomerRepositoryImpl();
		customer = new Customer(new CprNumber("1234"));
	}
	@Test(expected=NullPointerException.class)
	public void givenNullCustomerTryToRegister_returnNullPointerException() {
		repository.addCustomer(null);
	}
	@Test
	public void givenANewCustomerTryToRegister_customerAdded() {
		//arrange
		Customer customer = new Customer(new CprNumber("1234"));
		
		//act
		repository.addCustomer(customer);
		boolean customerFound = false;
		for(Customer c:repository.getCustomers()) 
			if(c.equals(customer)) customerFound = true;
		
		//assert
		assertTrue(customerFound);
	}
	
	@Test
	public void givenARegisteredCustomerTryContains_returnTrue() {
		//arrange
		Set<Customer> customers = new HashSet<>();
		customers.add(customer);
		repository.setCustomers(customers);
		
		//act
		//assert
		assertTrue(repository.contains(customer));
	}
	
	@Test
	public void givenANonRegisteredCustomerTryContains_returnFalse() {
		assertFalse(repository.contains(customer));
	}

}
