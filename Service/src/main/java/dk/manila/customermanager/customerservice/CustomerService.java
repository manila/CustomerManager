package dk.manila.customermanager.customerservice;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import dk.manila.customermanager.tools.TokenRepresentation;

/**
 * Interface representing a customer service
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 */
public interface CustomerService {

	/**
	 * Registers a customer in the CustomerService given his CPR number.
	 * 
	 * @param cpr
	 *            CPR number of the customer
	 * @throws IOException
	 *             if we cannot communicate correctly with the EventSender
	 * @throws TimeoutException
	 *             if we cannot communicate correctly with the EventSender
	 */
	void registerCustomer(String cpr) throws IOException, TimeoutException;

	/**
	 * Returns true if this CustomerService contains a customer with the specified
	 * cpr.
	 * 
	 * @param cpr
	 *            cpr whose presence in this CustomerService is to be tested
	 * @return true if this CustomerService contains a customer for the specified
	 *         cpr.
	 */
	boolean contains(String cpr);

	/**
	 * Returns a list of TokenRepresentation owned by a customer.
	 * 
	 * @param cpr
	 *            the cpr number of a customer
	 * @return a list of TokenRepresentation owned by a customer
	 * @throws IOException
	 * @throws TimeoutException
	 */
	Map<String, String>  getTokens(String cpr) throws IOException, TimeoutException;

}
