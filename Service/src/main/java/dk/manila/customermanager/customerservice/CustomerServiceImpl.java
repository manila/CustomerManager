package dk.manila.customermanager.customerservice;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeoutException;

import dk.manila.customermanager.queue.Event;
import dk.manila.customermanager.queue.EventReceiver;
import dk.manila.customermanager.queue.EventSender;
import dk.manila.customermanager.repository.CustomerRepository;
import dk.manila.customermanager.tools.CprNumber;
import dk.manila.customermanager.tools.Customer;

/**
 * Implementation of a CustomerService
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 */
public class CustomerServiceImpl implements CustomerService, EventReceiver {

	private CustomerRepository customerRepository;
	private EventSender eventSender;
	private CompletableFuture<Event> future;

	/**
	 * Constructor of the class CustomerServiceImpl
	 * 
	 * @param eventSender
	 *            EventSender used to send messages through a queue
	 * @param customerRepository
	 *            CustomerRepository where we save our customers
	 */
	public CustomerServiceImpl(EventSender eventSender, CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
		this.eventSender = eventSender;
	}

	/**
	 * Sets a new CustomerRepository used for testing purposed
	 * 
	 * @param repository
	 *            a CustomerRepository
	 */
	protected void setCustomerRepository(CustomerRepository repository) {
		this.customerRepository = repository;
	}

	@Override
	public void registerCustomer(String cpr) throws IOException, TimeoutException {

		CprNumber cprNumber = new CprNumber(cpr);

		Event event = new Event("addCustomerToTokenRepository");
		event.addArg(cpr);
		eventSender.sendEvent(event);

		Customer customer = new Customer(cprNumber);
		customerRepository.addCustomer(customer);
	}

	@Override
	public boolean contains(String customerCpr) {
		return customerRepository.contains(new Customer(new CprNumber(customerCpr)));
	}


	@Override
	public Map<String, String> getTokens(String cprString) throws IOException, TimeoutException {

		Event event = new Event("getTokensFromTokenRepositoryByCpr");
		event.addArg(cprString);
		future = new CompletableFuture<>();
		eventSender.sendEvent(event);

		Event callbackEvent = future.join();
		Map<String, String> tokensUriById = (Map<String, String>) callbackEvent.getArgs().get(0);
		return tokensUriById;		
	}

	@Override
	public void receiveEvent(Event event) {
		if (event.getInstruction().equals("getTokensFromTokenRepositoryByCprCallback")) {
			future.complete(event);
		} else if (event.getInstruction().equals("cpr_by_token_id_request_event_payment_to_customer")) {
			String tokenId = (String) event.getArgs().get(0);

			Event callbackEvent = new Event("cpr_by_token_id_request_event_customer_to_token");
			callbackEvent.addArg(tokenId);
			
			try {
				eventSender.sendEvent(callbackEvent);
			} catch (IOException | TimeoutException e) {
				e.printStackTrace();
			}
		} else if (event.getInstruction().equals("cpr_by_token_id_request_event_customer_to_tokenCallback")) {
			
			Event callbackEvent = new Event("cpr_by_token_id_request_event_payment_to_customerCallback");
			if (event.isSuccessful()) {
				String cpr = (String) event.getArgs().get(0);
				callbackEvent.addArg(cpr);
			} else {
				callbackEvent.setMessage(event.getMessage());
				callbackEvent.setSuccessful(false);
			}

			try {
				eventSender.sendEvent(callbackEvent);
			} catch (IOException | TimeoutException e) {
				e.printStackTrace();
			}
		}
	}
}
