package dk.manila.customermanager.queue;

/**
 * A receiver of events from a queue
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 * Inspired by the implementation given by Hubert Baumeister
 *
 */
public interface EventReceiver {

	/**
	 * Makes an action corresponding to a specified event.
	 * 
	 * @param event
	 *            a specified event
	 */
	void receiveEvent(Event event);
}
