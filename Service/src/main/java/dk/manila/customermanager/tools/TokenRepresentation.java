package dk.manila.customermanager.tools;

import java.io.IOException;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.io.output.ByteArrayOutputStream;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;

/**
 * Representation of a Token This class is used when a token needs to be
 * presented for a customer
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */

@XmlRootElement
public class TokenRepresentation {

	@XmlElement
	public String tokenId;
	@XmlElement
	public String barcodeUri;

	
	
	public TokenRepresentation(String tokenId, String barcodeUri) {
		this.tokenId = tokenId;
		this.barcodeUri = barcodeUri;
	}

	/**
	 * Gets the TokenId of the TokenRepresentation.
	 * 
	 * @return the TokenId of the TokenRepresentation
	 */
	public String getTokenId() {
		return tokenId;
	}

	/**
	 * Gets the barcode URI of the TokenRepresentation.
	 * 
	 * @return the barcode URI of the TokenRepresentation
	 */
	public String getBarcodeUri() {
		return barcodeUri;
	}
//
//	/**
//	 * Returns a byte array containing the image of a barcode representing the
//	 * tokenId
//	 * 
//	 * @return A byte array containing the image of a barcode representing the
//	 *         tokenId
//	 * @throws WriterException
//	 *             if we cannot write into the ByteArrayOutputStream
//	 * @throws IOException
//	 */
//	public byte[] generateBarcodeImage() throws WriterException, IOException {
//		int width = 250;
//		int height = 150;
//
//		BitMatrix barcode = new Code128Writer().encode(this.tokenId.toString(), BarcodeFormat.CODE_128, width, height);
//		ByteArrayOutputStream out = new ByteArrayOutputStream();
//		MatrixToImageWriter.writeToStream(barcode, "png", out);
//
//		return (byte[]) out.toByteArray();
//	}

}
