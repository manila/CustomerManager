package dk.manila.customermanager.tools;

/**
 * Class representing a CPR number
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public final class CprNumber {

	private final String cpr;

	/**
	 * Constructor of the class CprNumber
	 * 
	 * @param cpr
	 *            CPR number we want to save
	 */
	public CprNumber(String cpr) {
		this.cpr = cpr;
	}

	/**
	 * Gets the CPR number.
	 * 
	 * @return the CPR number
	 */
	public String getCpr() {
		return cpr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpr == null) ? 0 : cpr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CprNumber other = (CprNumber) obj;
		if (cpr == null) {
			if (other.cpr != null)
				return false;
		} else if (!cpr.equals(other.cpr))
			return false;
		return true;
	}

}
