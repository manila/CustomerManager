package dk.manila.customermanager.tools;

/**
 * Representation of a customer
 * 
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public final class Customer {

	private final CprNumber cprNumber;

	/**
	 * Constructor of the class Customer
	 * 
	 * @param cpr
	 *            CPR number of the customer
	 */
	public Customer(CprNumber cpr) {
		this.cprNumber = cpr;
	}

	/**
	 * Gets the CPR number of the customer
	 * 
	 * @return the CPR number of the customer
	 */
	public CprNumber getCprNumber() {
		return cprNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cprNumber == null) ? 0 : cprNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (cprNumber == null) {
			if (other.cprNumber != null)
				return false;
		} else if (!cprNumber.equals(other.cprNumber))
			return false;
		return true;
	}

}