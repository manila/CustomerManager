package dk.manila.customermanager.repository;

import java.util.HashSet;
import java.util.Set;

import dk.manila.customermanager.tools.Customer;

/**
 * Implementation of a CustomerRepository
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 */
public final class CustomerRepositoryImpl implements CustomerRepository {

	private Set<Customer> customers = new HashSet<>();

	@Override
	public void addCustomer(Customer customer) {
		if (customer == null)
			throw new NullPointerException();
		customers.add(customer);
	}

	@Override
	public boolean contains(Customer customer) {
		for (Customer c : customers)
			if (c.getCprNumber().equals(customer.getCprNumber()))
				return true;
		return false;
	}

	/**
	 * Returns a set of customers for testing purposes.
	 * 
	 * @return set of customers
	 */
	protected Set<Customer> getCustomers() {
		return new HashSet<>(customers);
	}

	/**
	 * Sets a set of customers in the CustomerRepositoryImpl for testing purposes.
	 * 
	 * @param customers
	 */
	protected void setCustomers(Set<Customer> customers) {
		this.customers = new HashSet<>(customers);
	}
}
