package dk.manila.customermanager.repository;

import dk.manila.customermanager.tools.Customer;

/**
 * A repository of customers
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 *
 */
public interface CustomerRepository {

	/**
	 * Adds a customer to the repository.
	 * 
	 * @param customer
	 *            customer to be added to the repository
	 */
	void addCustomer(Customer customer);

	/**
	 * Returns true if the repository contains the specified customer.
	 * 
	 * @param customer
	 *            customer whose presence in this repository is to be tested
	 * @return true if this repository contains the specified customer
	 */
	boolean contains(Customer customer);

}
