package dk.manila.customermanager.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dk.manila.customermanager.customerservice.CustomerService;
import dk.manila.customermanager.resources.CustomerResource;
import dk.manila.customermanager.tools.TokenRepresentation;

/**
 * Endpoint of a CustomerManager
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 */
@Path("/customers")
public class CustomerEndpointImpl {
	private CustomerService service;

	/**
	 * Constructor of a CustomerEndpointImpl
	 * 
	 * @param app
	 *            the application that creates the endpoint
	 */
	public CustomerEndpointImpl(@Context Application app) {
		service = (CustomerService) app.getProperties().get("service");
	}

	/**
	 * Calls the CustomerService to register a customer coming from a REST request.
	 * 
	 * @param customer
	 *            customer to be added in the CustomerManager
	 * @return an HTTP response 200 if the customer is registered
	 */
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createCustomer(CustomerResource customer) {
		try {
			service.registerCustomer(customer.cprNumber);
		} catch (IOException | TimeoutException e) {
			return Response.status(500).build();
		}
		return Response.ok().build();
	}

	/**
	 * Returns a list of TokenRepresentation corresponding to a given CPR number.
	 * 
	 * @param cpr
	 *            CPR number of the customer
	 * @return a list of TokenRepresentation corresponding to the given CPR number
	 */
	@GET
	@Path("/{cpr}/tokens")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerTokens(@PathParam("cpr") String cpr) {
		List<TokenRepresentation> tokenRepresentations;
		try {
			tokenRepresentations = makeTokenRepresentationsFromResponceStructure(service.getTokens(cpr));
		} catch (IOException | TimeoutException e) {
			return Response.status(500).build();
		}
		return new RestResponse(200, "Your tokens has been fetched - you have a total of " + tokenRepresentations.size() + " tokens",
				tokenRepresentations).getResponse();
	}

	private List<TokenRepresentation> makeTokenRepresentationsFromResponceStructure(Map<String, String> tokensUriById) {
		List<TokenRepresentation> tokensReprs = new ArrayList<>();
		for(String tokenId : tokensUriById.keySet()) 
			tokensReprs.add(new TokenRepresentation(tokenId, tokensUriById.get(tokenId)));		
		return tokensReprs;
	}
}
