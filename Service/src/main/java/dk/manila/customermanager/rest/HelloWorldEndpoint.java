package dk.manila.customermanager.rest;

import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * Endpoint used to ping if the service is working
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 */
@Path("/hello")
public class HelloWorldEndpoint {

	public HelloWorldEndpoint() {
	}

	/**
	 * Returns an HTTP response 200 if the service can be reached.
	 * 
	 * @return an HTTP response 200 if the service can be reached
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response doGet() {

		return Response.ok("Hello from Customer Manager!").build();
	}

}
