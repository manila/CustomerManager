package dk.manila.customermanager.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import dk.manila.customermanager.customerservice.CustomerService;
import dk.manila.customermanager.customerservice.CustomerServiceImpl;
import dk.manila.customermanager.queue.EventReceiver;
import dk.manila.customermanager.queue.RabbitMqListener;
import dk.manila.customermanager.queue.RabbitMqSender;
import dk.manila.customermanager.repository.CustomerRepositoryImpl;

/**
 * Application that starts a REST interface, creates a CustomerService and
 * starts the listener of queue.
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 */
@ApplicationPath("/")
public class RestApplication extends Application {
	private final String EXCHANGE_NAME = "dtu_pay_exchange";
	private final String EXCHANGE_HOST = "rabbitmq";
	private final String EXCHANGE_TYPE = "fanout";
	private CustomerService service;

	/**
	 * Constructor of RestApplication in which we start a REST interface, create a
	 * CustomerService and start the listener of queue.
	 */
	public RestApplication() {

		service = new CustomerServiceImpl(new RabbitMqSender(EXCHANGE_NAME, EXCHANGE_HOST, EXCHANGE_TYPE),
				new CustomerRepositoryImpl());
		try {
			new RabbitMqListener((EventReceiver) service).listen(EXCHANGE_NAME, EXCHANGE_HOST, EXCHANGE_TYPE);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("service", service);
		return map;
	}

}
